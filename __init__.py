import urllib.request
import json
import hashlib

from rdflib import Graph, Literal, URIRef, BNode, Namespace
from rdflib.namespace import FOAF, RDF, DC, DOAP

from . import secrets

CV = Namespace('http://rdfs.org/resume-rdf/cv.rdfs#')

META = Namespace('http://pafcu.fi/ns/meta/')
def triple_uri(triple):
	return URIRef('triple:sha1:%s'%hashlib.sha1(bytes(str(triple),'utf-8')).hexdigest())

def get_profile(gitlab_id):
	g = Graph()
	base_url = 'https://gitlab.com/api/v3'
	user_url = '%s/users/%s'%(base_url, gitlab_id)

	user_uri = URIRef(user_url)
	with urllib.request.urlopen(user_url+'?private_token=%s'%secrets.private_token) as f:
		user_profile = json.loads(f.read().decode('utf-8'))

	if user_profile.get('name', None): g.add((user_uri, FOAF.name, Literal(user_profile['name'])))
	if user_profile.get('email', None): g.add((user_uri, FOAF.mbox, URIRef('mailto:' + user_profile['email'])))
	if user_profile.get('bio', None): g.add((user_uri, CV.olb, Literal(user_profile['bio'])))

	for triple in g:
		g.add((triple_uri(triple), META.sourceService, URIRef('https://gitlab.com/')))

	return g
